<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&amp;display=swap"
        rel="stylesheet">
    <title>Задание 5</title>
</head>

<body>


    <?php
	    if (!empty($result_message)) {
	        print('<div id="messages">');
	        foreach ($result_message as $message) {
		        print($message);
	        }
	        print('</div>');
	      }
    ?>


    <?php
        if(empty($_SESSION['login'])){
            print('Вы можете <a href="login.php">войти</a> под своим логином и паролем для изменения данных.');
        } else print('Вы вошли в режим изменения данных');
    ?>


    <form method="POST" id="form" action="index.php">
        <div class="form-inner">
            <h3>Расскажите о себе</h3>
            <label>Имя:<br />
                <input type=text name="fio" placeholder="Введите ваше имя" <?php if($errors['fio']){print 'class = "error"';}?>
                value="<?php print $values['fio']; ?>" />
            </label><br />

            <label>E-mail:<br />
                <input name="email" placeholder="Введите ваш e-mail" type="email" 
                <?php if($errors['email']){print 'class = "error"';}?>
                value="<?php print $values['email']; ?>">
            </label><br />

            <label>Год рождения:<br />
                <input type="date" name="date" <?php if($errors['date']){print 'class = "error"';}?>>
            </label><br />

            <label>Пол:</label><br />
            <label class="radio"><input type="radio" name="sex" value="1"  <?php if ($values['sex'] == '1') {print 'checked';} ?>/>Мужской
            </label>
            <label class="radio"><input type="radio" name="sex" value="0"<?php if ($values['sex'] == '0') {print 'checked';} ?> />Женский
            </label><br />

            <label>Выберите кол-во конечностей:</label><br />
            <label class="radio"><input type="radio" name="limbs" value="0" <?php if ($values['limbs'] == '0') {print 'checked';} ?>/>0
            </label>
            <label class="radio"><input type="radio" name="limbs" value="1" <?php if ($values['limbs'] == '1') {print 'checked';} ?> />1
            </label>
            <label class="radio"><input type="radio" name="limbs" value="2" <?php if ($values['limbs'] == '2') {print 'checked';} ?> />2
            </label>
            <label class="radio"><input type="radio" name="limbs" value="3" <?php if ($values['limbs'] == '3') {print 'checked';} ?> />3
            </label>
            <label class="radio"><input type="radio" name="limbs" value="4" <?php if ($values['limbs'] == '4') {print 'checked';} ?> />4
            </label><br />

            <label>Ваши сверхспособности:<br />
                <select multiple="true" name="superpower[]">
                    <option value="Бессмертие" <?php if ($values['superpower']['0']) {print 'selected';} ?>>Бессмертие</option>
                    <option value="Прохождение сквозь стены" <?php if ($values['superpower']['1']) {print 'selected';} ?>>Прохождение сквозь стены</option>
                    <option value="Левитация" <?php if ($values['superpower']['2']) {print 'selected';} ?>>Левитация</option>
                </select>
            </label><br />

            <label>
                Биография:<br />
                <textarea name="biography" placeholder="Расскажите о себе"></textarea>
                <br />
            </label>

            <label <?php if ($errors['check']) {print 'class="error"';} ?>>
                <input name="check" type="checkbox" checked=checked value=1>С контрактом ознакомлен:<br />
            </label>

            <input type="submit" value="Отправить" />
        </div>
    </form>

    </div>

</body>

</html>
