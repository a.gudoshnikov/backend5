<?php



// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  session_destroy();
  header('Location: index.php');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
if (!empty($_GET['no_such_login']))
print("<div>Пользователь с таким логином отсутствует в базе данных</div>");
if (!empty($_GET['wrong_pass']))
print("<div>Кажется, вы ввели неверный пароль</div>");

?>
<form action="" method="post">
  <input name="login" />
  <input name="pass" />
  <input type="submit" value="Войти" />
</form>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  $conn = new PDO("mysql:host=localhost;dbname=u41048", 'u41048', '1308338', array(PDO::ATTR_PERSISTENT => true));

  // ???????????????????????????????????????????????????????????

  $user = $conn->prepare('SELECT id, u_pass FROM form WHERE u_login = ?'); // запрос на выборку
  $user -> execute([$_POST['login']]);
  $row = $user->fetch(PDO::FETCH_ASSOC);

  if(!$row){
    header('Location: ?no_such_login=1');
    exit();
  }

  if(md5($_POST['pass']) != $row['u_pass']){
    header('Location:?wrong_pass=1');
    exit();
  }
      

  // Если все ок, то авторизуем пользователя.
  $_SESSION['login'] = $_POST['login'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = $row['id'];
  $errors['pass'] = false;
  $errors['login'] = false;
  // Делаем перенаправление.
  header('Location: index.php');
}
