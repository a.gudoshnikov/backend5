<?php
//Функция генерации случайной последовательности букв и цифр заданной длинны
function gen_password($length)
{
	$password = '';
	$arr = array(
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
		'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
		'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
		'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
	);
 
	for ($i = 0; $i < $length; $i++) {
		$password .= $arr[random_int(0, count($arr) - 1)];
	}
	return $password;
}

header('Content-Type: text/html; charset=UTF-8');

//Создаём массив символов, в котором будем хранить наше сообщение

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $result_message = array();

    //Если в массиве позиция save не является пустой, то удаляем cookie и записываем в сообщение результат
    if(!empty($_COOKIE['save'])){
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $result_message[] = 'Спасибо, результаты сохранены и занесены в базу.';
    }

    if(!empty($_COOKIE['pass'])){
        $result_message[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }

    //Создаём массив ошибок
    //Создаём элемент в массиве error
    //Если элемент непустой, то запишем в результирующий массив сообщение и удаляем cookie
    //Аналогично для каждого поля

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    if($errors['fio']){
        setcookie('fio_error', '', 100000);
        $result_message[] = '<div> Заполните имя.</div>';
    }

    $errors['email'] = !empty($_COOKIE['email_error']);
    if($errors['email']){
        setcookie('email_error', '', 100000);
        $result_message[] = '<div> Заполните email.</div>';
    }

    $errors['date'] = !empty($_COOKIE['date_error']);
    if($errors['date']){
        setcookie('date_error', '', 100000);
        $result_message[] = '<div> Заполните дату рождения.</div>';
    }
    
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    if($errors['sex']){
        setcookie('sex_error', '', 100000);
        $result_message[] = '<div> Выберите ваш пол.</div>';
    }

    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    if($errors['limbs']){
        setcookie('limbs_error', '', 100000);
        $result_message[] = '<div> Выберите количество конечностей.</div>';
    }

    $errors['check'] = !empty($_COOKIE['check_error']);
    if($errors['check']){
        setcookie('check_error', '', 100000);
        $result_message[] = '<div> Вы должны принять условия контракта.</div>';
    }

    //Создаём массив значений наших полей 
    //Используя условные выражения проверим существование этих значений 
    //И в зависимости от этого либо запишем в каждый элемент values пустую строку, либо
    //перепишем значение из формы
    $values = array();
    $values['fio'] = empty($_COOKIE['value_of_fio']) ? '' : $_COOKIE['value_of_fio'];
    $values['email'] = empty($_COOKIE['value_of_email']) ? '' : $_COOKIE['value_of_email'];
    $values['date'] = empty($_COOKIE['value_of_date']) ? '' : $_COOKIE['value_of_date'];
    $values['sex'] = empty($_COOKIE['value_of_sex']) ? '' : $_COOKIE['value_of_sex'];
    $values['limbs'] = empty($_COOKIE['value_of_limbs']) ? '' : $_COOKIE['value_of_limbs'];
    $values['check'] = empty($_COOKIE['value_of_check']) ? '' : $_COOKIE['value_of_check'];
    $values['superpower'] = array();
    $values['superpower'][0] = empty($_COOKIE['superpower_0']) ? '' : $_COOKIE['superpower_0'];
    $values['superpower'][1] = empty($_COOKIE['superpower_1']) ? '' : $_COOKIE['superpower_1'];
    $values['superpower'][2] = empty($_COOKIE['superpower_2']) ? '' : $_COOKIE['superpower_2'];
    
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {

        $conn = new PDO("mysql:host=localhost;dbname=u41048", 'u41048', '1308338', array(PDO::ATTR_PERSISTENT => true));

        $user = $conn->prepare('SELECT fio, email, your_date, sex, limbs, bio, accept FROM form WHERE id = ?'); // запрос на выборку
        $user -> execute([$_SESSION['uid']]);
        $row = $user->fetch(PDO::FETCH_ASSOC);
        $values['fio'] = $row['fio'];
        $values['email'] = $row['email'];
        $values['date'] = $row['date'];
        $values['sex'] = $row['sex'];
        $values['limbs'] = $row['limbs'];
        $values['check'] = $row['check'];
        $values['biography'] = $row['biography'];
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
       
    }
    include('form.php');
}
else {
    $errors = false;
//Проверка на корректность заполнения полей

if(empty($_POST['fio'])){
    setcookie('fio_error', '1', time() + 24*60*60);
    $errors = true;
}
else{
    setcookie('value_of_fio', $_POST['fio'], time() + 30*24*60*60);
}

if(!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['email'])){
    setcookie('email_error', '1', time() + 24*60*60);
    $errors = true;
}
else{
    setcookie('value_of_email', $_POST['email'], time() + 30*24*60*60);
}

if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
    setcookie('date_error', '1', time() + 24*60*60);
    $errors = true;
}else{
    setcookie('value_of_date', $_POST['date'], time() + 30*24*60*60);
}

if (!preg_match('/^[0-1]$/', $_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('value_of_sex', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }

  if (!preg_match('/^[0-4]$/', $_POST['limbs'])) {
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('value_of_limbs', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }

  if (!isset($_POST['check'])) {
    setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('value_of_check', $_POST['check'], time() + 30 * 24 * 60 * 60);
  }

  foreach($_POST['superpower'] as $sup) {
    setcookie('superpower_value_' . $sup, 'true', time() + 30 * 24 * 60 * 60);
    }

    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else{
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('date_error', '', 100000);
        setcookie('check_error', '', 100000);
    }
    if(!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])){
        $conn = new PDO("mysql:host=localhost;dbname=u41048", 'u41048', '1308338', array(PDO::ATTR_PERSISTENT => true));

        $user = $conn->prepare("UPDATE form SET fio = ?, email = ?, your_date = ?, sex = ?, limbs = ?, bio = ?, accept = ? WHERE id = ?");
        $user -> execute([$_POST['fio'], $_POST['email'], date('Y-m-d', strtotime($_POST['date'])), $_POST['sex'], $_POST['limbs'], $_POST['biography'], $_POST['check'], $_SESSION['uid']]);
        
        $user1 = $conn->prepare('DELETE FROM superpowers WHERE id = ?');
        $user1 -> execute([$_SESSION['uid']]);
        
        $id_user = $_SESSION['uid'];
        $user2 = $conn->prepare("INSERT INTO superpowers SET id = ?, your_power = ?");
        foreach ($_POST['superpower'] as $sup)
            $user2 -> execute([$id_user, $sup]);

            setcookie('value_of_fio', '', 100000);
            setcookie('value_of_email', '', 100000);
            setcookie('value_of_date', '', 100000);
            setcookie('value_of_sex', '', 100000);
            setcookie('value_of_limbs', '', 100000);
            setcookie('value_of_check', '', 100000);
        header('Location: login.php');    
    } else{

    $login = gen_password(10);
    $pass = gen_password(10);

    setcookie('login', $login);
    setcookie('pass', $pass);

    $pass = md5($pass);

    $conn = new PDO("mysql:host=localhost;dbname=u41048", 'u41048', '1308338', array(PDO::ATTR_PERSISTENT => true));

    try{
    
    $user = $conn->prepare("INSERT INTO form SET fio = ?, email = ?, your_date = ?, sex = ?, limbs = ?, bio = ?, accept = ?, u_login = ?, u_pass = ?");
    $user -> execute([$_POST['fio'], $_POST['email'], date('Y-m-d', strtotime($_POST['date'])), $_POST['sex'], $_POST['limbs'], $_POST['biography'], $_POST['check'], $login, $pass]);
   
    $id_user = $conn->lastInsertId();
    $user1 = $conn->prepare("INSERT INTO superpowers SET id = ?, your_power = ?");
    foreach ($_POST['superpower'] as $sup)
        $user1 -> execute([$id_user, $sup]);
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
setcookie('value_of_fio', '', 100000);
setcookie('value_of_email', '', 100000);
setcookie('value_of_date', '', 100000);
setcookie('value_of_sex', '', 100000);
setcookie('value_of_limbs', '', 100000);
setcookie('value_of_check', '', 100000);
setcookie('save', '1');
header('Location: index.php');
}
}

?>